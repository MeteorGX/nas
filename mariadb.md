用于部署`MariaBD`/`MySQL`数据库来作为数据容器,也可以安装`PostageSQL`,这里部署服务器都是使用dnf源安装而不是用手动编译来处理,避免要编写大量的`systemctl服务`.

# 安装MariaDB

这里安装直接使用`dnf`命令就行了:

```
$> dnf install mariadb mariadb-server
```

这里会自动安装所有`mariadb`依赖,安装之后启动并且设置开机启动.

```
$> systemctl start mariadb.service
$> systemctl enable mariadb.service
```
____

# 配置MariaDB

这里需要修改配置文件让`MariaDB`达到最高性能:

>`/etc/my.cnf.d/mariadb-server.cnf`

```
# 系统默认不需要更改
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mariadb/mariadb.log
pid-file=/run/mariadb/mariadb.pid

# 以下推荐默认配置

# 只有内网访问需要写成内网IP,或者不需要开放直接127.0.0.1即可
bind-address=0.0.0.0

# 端口内网直接取默认即可
port=3306

# 默认引擎
default-storage-engine=innodb 

# 默认字符串
character-set-server=utf8mb4
collation-server=utf8mb4_general_ci 

# 表缓存
table_cache=65535
table_definition_cache=65535
​
max_allowed_packet=4M
net_buffer_length=1M
bulk_insert_buffer_size=16M

# 引擎搜索配置
query_cache_type=1 # 是否启用查询缓存,如果表写入状况多就设置为0关闭查缓存
query_cache_size=16M # 查询缓存大小,开启会消耗CPU来创建缓存命中,同上关闭设为0

# 引擎设置
key_buffer_size=8M # 用于MyISAM引擎的索引大小
myisam_sort_buffer_size=32M # 用于MyISAM的排序大小
innodb_buffer_pool_size=2G # 用于Innodb的缓存,设置为内存15%/25%/50%配置

max_heap_table_size=16M # 用于中间表大小
tmp_table_size=16M # 用于中间临时的表大小

# 单次查询限制
sort_buffer_size=256K # 排序缓存的大小
read_buffer_size=128K # 为需要全表查询的MyISAM引擎线程指定读取的缓存大小
read_rnd_buffer_size=4M # 对已经排序的表读取时的缓存,内存选择可以为2M(小)/4M(中)/6M(大)
join_buffer_size=1M # 对Join的表连接查询缓存大小,1~2M之中取值
thread_stack=256K # 线程空间大小,256K/512K取值
binlog_cache_size=64K # 事务的缓存配置

# 线程配置
thread_cache_size=4 # 线程总占用大小
thread_concurrency=8 # 创建多少线程

# 大表设置
innodb_file_per_table=1
skip-external-locking
```

配置完之后重启`MariaDB即可`,

____

# 开放外部访问端口

还需要设置开放端口配置,直接参照`firewall-cmd`开放对应端口
```
$> firewall-cmd --zone=public --add-port=3306/tcp --permanent
$> firewall-cmd --reload
```
____

# 初始化数据库

```
# 初始化配置命令
$> mysql_secure_installation

# 如下选择:

Enter current password for root (enter for none) 
(如果当前Root密码,第一次进去默认空回车即可)

Set root password? [Y/n]
(是否配置root密码,最好Y进行设置root)

Remove anonymous users? [Y/n]
(是否删除游客账号,必须删除掉内部一些游客账号)

Disallow root login remotely?
(是否禁用Root可以被远程连接,建议禁用)

Remove test database and access to it? [Y/n]
(是否删除数据库内部的test数据库,建议删除这些测试留下的表)

Reload privilege tables now? [Y/n]
(是否立即刷新权限表,需要立即刷新生效刚刚权限配置)
```

确认之后就完成初始化配置,可以通过命令来进入`MariaDB`:

> `mysql -uroot -p` # 输入密码

____

# 创建对外访问的账号

```
# 1.登陆Mysql内部进行配置如下二选一:

# 创建局域网/本地访问账号(127.0.0.1改成内网本机服务器IP)
create user meteorcat@'127.0.0.1' identified by 'asd123456';

# 创建可远程访问账号
create user meteorcat@'%' identified by 'asd123456';


# 2.如果修改密码错误可以修改
set password for 'meteorcat'@'%' = password('qwe123456');

# 3.授权给全网访问

# 全部数据库都进行可访问
grant all privileges on *.* to meteorcat;

# 指定某些数据库可以访问
grant all privileges on 数据库名称.* to meteorcat;

# 4.更新权限表信息
flush privileges ;
```

之后就能通过外网来进行访问数据库配置了,这里的对外设置尽量不要弄成外网,而且使用内网IP或者127.0.0.1

____

# 备份还原数据库

```
# 备份全数据库命令
/usr/bin/mysqldump -uroot -p -x  --default-character-set=utf8mb4 --flush-logs --all-databases > /tmp/mariadb.sql

# 备份指定数据库
/usr/bin/mysqldump -uroot -p --default-character-set=utf8mb4 --flush-logs 数据库名 > /tmp/mariadb.sql

# 还原数据库信息
/usr/bin/mysql -uroot -p --default-character-set=utf8mb4 < /tmp/mariadb.sql

```

> 数据库备份可以做成定时脚本定时处理