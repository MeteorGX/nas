# NAS

> NAS家用服务器环境搭建要点, 用于部署个人家用服务器

* [虚拟化方案](virtua.md)

* [系统选择](system.md)

* [虚拟机安全配置](init.md)

* [SSH密钥登陆](ssh.md)

* [搭建MariaDB](mariadb.md)

* [搭建Redis](redis.md)

* [搭建MongoDB](mongo.md)

* [搭建rsyslog日志中心](rsyslog.md)

* [搭建rsync同步中心](rsync.md)

* [搭建Gitea版本控制](gitea.md)