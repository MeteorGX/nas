让`SSH`支持密钥登陆是十分有必要的,可以避免很大部分对于`SSH`的密码爆破,直接避免大部分的安全漏洞问题.

# 生成密钥

这一步十分简单,直接再需要登陆的用户下执行命令即可

```
#-t 代表密钥类型 -b代表密钥长度 -C是一个注释说明
$> ssh-keygen  -t rsa -b 4096 -C "your_email@domain.com"

#直接输入如下:
Enter file in which to save the key (/home/meteorcat/.ssh/id_rsa):
(输入保存密钥的位置,默认回车即可)

Enter passphrase (empty for no passphrase):
(输入密钥之后的加密密码,如果输入就不能免密钥登陆,默认回车)

Enter same passphrase again: 
(再次输入加密密码,如果空回车即可)
```

这样就会在你的登陆用户家目录里出现生成的密钥:
```
/home/执行的用户/.ssh
#内部包含两个文件:
id_rsa(私钥,用户登陆需要确认的密钥)
id_rsa.pub(公钥,放置服务器进行登陆校对)
```

____

# 配置authorized_keys

>注意: 对于连接服务器之中并不需要服务器的两个文件,虽然能够拷贝服务器的`id_rsa`到本地使用,但是及其不推荐!

____

## 客户端需要的操作:

直接需要登陆的客户端按上面方法再次生成密钥,获取到`id_rsa.pub`,这个就是我们需要用到的客户登陆凭证,而不是服务器生成的`id_rsa`/`id_rsa.pub`!


____

## 服务端需要的操作

在最开始生成`ssh`密钥的用户下创建文件`authorized_keys`.

```
#创建对应的公钥保存文件
$> touch ~/.ssh/authorized_keys

#这个文件需要权限只有自己能够看到
$> chmod 600 ~/.ssh/authorized_keys


#将数据客户端公钥放入其中,一行占据一个公钥
#类似:MLERUxtB..........(省略若干字符) meteorcat@vm.dev
#
$> cat 客户端公钥 >> ~/.ssh/authorized_keys
```

____

# 启用密钥登陆

> 修改文件: `/etc/ssh/sshd_config`

```
#开启公钥验证登陆
PubkeyAuthentication yes

#确认authorized_keys目录
AuthorizedKeysFile      .ssh/authorized_keys
```

重启`SSH`服务之后尝试下是否能够免密钥直接密钥登陆.

>千万一定不要连同密码登陆关闭一起启用,避免登陆某个操作失误直接完全无法登陆!

____

# 禁用密钥登陆

这个操作也可以加强服务器安全设置,直接避免对`SSH`的密码爆破:

> 修改文件: `/etc/ssh/sshd_config`

```
# 关闭密码登陆
PasswordAuthentication no
```

这一步也需要重启`SSH`服务器

____

# 其他的SSH配置操作

实际上还有通过指定用户绕过ssh等操作,这后续会补充上.



