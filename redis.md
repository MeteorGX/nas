`Redis`最好部署5.0版本以上的, 因为内部加入了`Stream`方式来作为消息队列服务器,从此也就能利用`Redis`作为队列消息功能部署. 还可以类似`memcache`一样使用`session`缓存数据库.

> `memcache`已经不推荐安装, 而是应该用`Redis`来替代其功能.

# 安装Redis

这里也是直接使用包安装:

```
$> dnf install redis
```

设置启动并且开机重启

```
$> systemctl start redis.service
$> systemctl enable redis.service
```

____

# 配置Redis

实际上`Redis`并没有什么需要配置的大问题,主要集中在绑定的地址端口/内存设置/数据同步到磁盘上.

> 修改文件: `/etc/redis.conf`

```
# 允许本地访问
#bind 127.0.0.1
# 允许内网访问
#bind 192.168.0.x
# 允许公网访问
bind 0.0.0.0

# 绑定端口,默认配置即可
port 6579

# 配置守护模式
#daemonize no
daemonize yes

# 指定守护模式对象
#supervised no
supervised systemd

# 这里是redis数据落地/日志等保存的文件夹;必须留意这里,因为数据落地备份文件就是在此
dir /var/lib/redis

# 设置访问密码,请注意最好使用md5之后的密码,因为Redis内存运算能力速度太快很容易被爆破密码
#requirepass foobared
requirepass qwertyuiop1234567

# 数据落地备份,这里会周期性按照配置将数据落地备份,备份文件默认为appendonly.aof
#appendonly no
appendonly yes
appendfilename "appendonly.aof"

# 数据落地规则,everysec是指定间隔周期数据落地,综合性能和效率中等
appendfsync everysec
```

配置完成直接重启`Redis`即可.

# 备份还原落地数据

直接复制`Reids`之中的`dir`配置文件夹之中的文件即可:

> `/var/lib/redis/appendonly.aof` # 这就是落地的备份数据库

```
# 将数据备份导出
$> cp /var/lib/redis/appendonly.aof /tmp/appendonly.aof
```


而还原数据更加简单:

```
# 先停止Redis服务.
$> systemctl stop redis.service

# 将原先落地数据备份
$> cp /var/lib/redis/appendonly.aof /var/lib/redis/appendonly.bak

# 替换原来的数据导入
$> mv /tmp/appendonly.aof /var/lib/redis/appendonly.aof

# 启动之后会自动导入落地数据
$> systemctl start redis.service
```
____
