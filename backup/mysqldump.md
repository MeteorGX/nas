# 备份数据

> 执行命令 : ``/usr/bin/mysqldump -uroot -p -x  --flush-logs --all-databases > /tmp/mariadb_`date +"%Y-%m-%d"`.sql``


# 还原数据

> 执行命令 : `/usr/bin/mysql -u root -p < all-databases.sql`

