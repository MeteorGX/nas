## 开启备份

> 编辑文件: `sudo vim /etc/redis.conf`

<pre>
......
supervised systemd
appendonly yes
appendfilename "appendonly.aof"
appendfsync everysec
......
</pre>

>重启之后,会自动在生成备份文件:`/var/lib/redis/appendonly.aof`

____

## 还原备份

> 关闭服务: `sudo systemctl stop redis`

> 编辑文件: `sudo vim /etc/redis.conf`

先关闭自动备份
<pre>
......
appendonly no
......
</pre>

> 数据备份放回: `cp appendonly.aof /var/lib/redis/`

> 启动服务: `sudo systemctl start redis`