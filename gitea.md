> 这里使用`gitea`不打算使用`docker`容器搭建,而是作为系统服务写入虚拟机之中.


# 前置准备

* 直接源安装git

```
$> sudo dnf install git -y
```

* 首先需要建立一个账号来作为运行`gitea`的对象:

```
#添加一个git用户和分组作为运行对象,千万不要用root等管理员作为运行对象
$> sudo adduser git
```

* 将该对象设定成不可以ssh登陆,这是十分有必要的!

> 修改文件: `/etc/passwd`

```
......
#找到git用户那一栏修改成git-shell -> /usr/bin/git-shell
git:x:1003:1003::/home/git:/usr/bin/git-shell
......
```

这样前置准备基本就完成了.

____

# 下载Gitea最新版

直接在`/tmp`目录运行命令

```
#这个地址有时候无法访问,需要多试几次
$> wget -O gitea https://dl.gitea.io/gitea/master/gitea-master-linux-amd64

#下载下来的gitea就是执行文件,需要让他可执行
$> sudo chmod +x gitea

#将其放入系统全局的可执行目录之中
$> sudo mv gitea /usr/bin

#将这个gitea设置为root对象所有
$> sudo chown root:root /usr/bin/gitea

#生成作为gitea配置的文件和文件夹目录
$> sudo mkdir /var/lib/gitea

#将目录权限转移给git
$> sudo chown -R git:git /var/lib/gitea

```

这样就能获取到最新版`gitea`,可以通过命令来测试查看是否输出:

```
$> gitea --help 
```

____


# 编写systemctl服务

> 直接编辑文件: `sudo vim /etc/systemd/system/gitea.service`

```
[Unit]
Description=Gitea for MeteorCat 
After=syslog.target
After=network.target


[Service]
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
# If using Unix socket: tells systemd to create the /run/gitea folder, which will contain the gitea.sock file
# (manually creating /run/gitea doesn't work, because it would not persist across reboots)
#RuntimeDirectory=gitea
ExecStart=/usr/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
# If you want to bind Gitea to a port below 1024, uncomment
# the two values below, or use socket activation to pass Gitea its ports as above
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE
###

[Install]
WantedBy=multi-user.target
```

首先测试一下是否能运行,这一步是必须的,主要是为了能够生成所需要的配置文件:

```
$> sudo /usr/bin/gitea web --config /etc/gitea/app.ini

#运行之后等待一下直接Ctrl+D中断即可,此时已经生成完配置文件
#可以通过命令查看是否生成:
$> ls -l /etc/gitea/app.ini

#确认生成之后,需要将这个归属为git用户
$> sudo chown git:git /etc/gitea/app.ini
```

更新`systemctl`的服务并且启动查看`gitea`:

```
#更新所有服务器
$> sudo systemctl daemon-reload

#启动Gitea
$> sudo systemctl start gitea

#查看状态
$> sudo systemctl status gitea

#开机启动
$> sudo systemctl enable gitea
```

这时候服务器如果没有什么错误一般就是已经启动了,这时候就是访问网页服务进行安装配置:

> 默认为:`http://127.0.0.1:3000`,配置好之后才能继续下一步的设置.


____

# 配置细化

具体完全配置可以参考[`官网`](https://docs.gitea.io/zh-cn/config-cheat-sheet/)

配置文件已经生成在`/etc/gitea/app.ini`,此时需要对其配置修改一下:

```
#这里挑选几个比较关键的配置:

[server]
#默认进入页面,直接到public项目页面
LANDING_PAGE=explore 

#Indexer选项,这个配置待定,Database之手
[indexer]
ISSUE_INDEXER_TYPE=bleve
ISSUE_INDEXER_PATH=indexers/issues.bleve
ISSUE_INDEXER_QUEUE_TYPE=redis
ISSUE_INDEXER_QUEUE_CONN_STR= network=tcp addrs=127.0.0.1:6379 password=qwe123456 db=0 
ISSUE_INDEXER_QUEUE_BATCH_NUMBER=20
REPO_INDEXER_ENABLED=true
REPO_INDEXER_PATH=indexers/repos.bleve
UPDATE_BUFFER_LEN=20
MAX_FILE_SIZE=1048576


#Cache选项,openid之后
[cache]
ADAPTER=redis
INTERVAL=60
HOST=network=tcp,addr=127.0.0.1:6379,password=qwe123456,db=0,pool_size=10,idle_timeout=180
ITEM_TTL=16h

#Session选项,cache选项之后
[session]
PROVIDER    =   redis
PROVIDER_CONFIG =   network=tcp,addr=127.0.0.1:6379,password=qwe123456,db=0,pool_size=10,idle_timeout=180

#其他选项,log选项之后
[other]
SHOW_FOOTER_BRANDING    =   false
SHOW_FOOTER_VERSION =   false
SHOW_FOOTER_TEMPLATE_LOAD_TIME  =   false

#Task选项,other选项之后
[task]
QUEUE_TYPE  =   redis
QUEUE_LENGTH = 100
QUEUE_CONN_STR = network=tcp addrs=127.0.0.1:6379 password=qwe123456 db=0 
```

____

# Nginx反向代理

如果您已经有一个域名并且想与 `Gitea` 共享该域名,那么可以单独构建个 `conf` 来配置代理http/https转发.

> 注意: 这里确保安装了 `Nginx`
>> 命令: `dnf install nginx`

新增/修改文件: `sudo vim /etc/nginx/conf/gitea.conf`

```
server {
    # 默认http端口
    listen 80;

    # 域名
    server_name git.example.com;

    # 转发
    location / { # Note: Trailing slash
        proxy_set_header  Host  $host;
        proxy_set_header  X-real-ip $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://localhost:3000; # Note: Trailing slash
    }
}
```

注意配置文件也需要修改设置:

编辑文件: `sudo vim /etc/gitea/app.ini`

```
[server]
ROOT_URL = http://git.example.com
```

____

# 备份/还原数据

`Gitea`支持数据备份和导入,这样可以直接保证数据不会因为丢失.


```
#备份命令,会在所在目录生成一个gitea-dump-xxxxx.zip压缩包
$> sudo -u git /usr/bin/gitea dump -c /etc/gitea/app.ini



#注意目前没有自动导入命令,需要自己手动导入

#获得压缩包首先需要解压
$> sudo unzip gitea-dump-xxxxx.zip

#进入压缩包内部
$> cd gitea-dump-xxxxx

#配置移动app.ini
$> sudo mv custom/conf/app.ini /etc/gitea/app.ini

#移动仓库数据
$> sudo mv gitea-repo/* /home/git/gitea-repositories

#更改权限
$> sudo chown -R git:git /etc/gitea/app.ini /home/git/gitea-repositories

#数据库还原[mariadb]
$> sudo mysql -u账号 -p密码 数据库名称 <gitea-db.sql

#重启服务
$> sudo systemctl restart gitea
```

____

# 定时 `systemctl` 备份

编写service服务 : `sudo vim /etc/systemd/system/gitea-backup.service`

```
[Unit]
Description=Gitea Backup

[Service]
Type=simple
User=git
Group=git

#备份目录,必须先创建好并把权限给git
#sudo mkdir /var/lib/gitea/backdup
#sudo chown git:git /var/lib/gitea/backdup
WorkingDirectory=/var/lib/gitea/backdup


ExecStart=/usr/bin/gitea dump -c /etc/gitea/app.ini
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea

[Install]
WantedBy=multi-user.target
```

编写定时器: `sudo vim /etc/systemd/system/gitea-backup.timer`

```
[Unit]
Description=Daily Gitea Backup

[Timer]
# See systemd.time(7) for time format
# 每天4点执行
OnCalendar=*-*-* 04:00:00
#持续工作,避免执行一次退出
Persistent=true

[Install]
WantedBy=timers.target
```

同步systemctl服务,启动定时器,并且让他开机启动:

```
#同步
$> sudo systemctl daemon-reload

#启动定时器,服务器没必要开启,定时器会自动触发
$> sudo systemctl start gitea-backup.timer

#开机启动
$> sudo systemctl enable gitea-backup.timer
```


