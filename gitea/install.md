# 部署系统服务Gitea

## 注册一个系统用户

> `sudo adduser git`
>> 这个账号用于常用Git登陆用户


## 不让用于登陆SSH
> `sudo vim /etc/passwd`
<pre>
.......
找到文件对应的行
`git:x:1003:1003::/home/git:/bin/bash`
修改成以下
`git:x:1003:1003::/home/git:/usr/bin/git-shell`
.......
</pre>


## 下载Gitea最新版本
> 下载二进制文件 : `wget -O gitea https://dl.gitea.io/gitea/master/gitea-master-linux-amd64`
>> 下载完成之后注意将其设置为可执行文件:    
>> `chmod +x gitea`

## 移动可执行对象
> `sudo mv gitea /usr/bin`


## 编写servic文件
> `sudo vim /etc/systemd/system/gitea.service`
<pre>
[Unit]
Description=Gitea for MeteorCat 
After=syslog.target
After=network.target


[Service]
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
# If using Unix socket: tells systemd to create the /run/gitea folder, which will contain the gitea.sock file
# (manually creating /run/gitea doesn't work, because it would not persist across reboots)
#RuntimeDirectory=gitea
ExecStart=/usr/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
# If you want to bind Gitea to a port below 1024, uncomment
# the two values below, or use socket activation to pass Gitea its ports as above
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE
###

[Install]
WantedBy=multi-user.target
</pre>

>注意:最好先启动一下生成必要的文件目录:
>> `/usr/bin/gitea web --config /etc/gitea/app.ini`



## 更新Systemctl
> `sudo systemctl daemon-reload`

## 启动服务器
> `sudo systemctl start gitea.service`







