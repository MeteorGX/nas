# 句柄数量

> 修改文件: `sudo vim /etc/security/limits.conf`

<pre>
.......
* soft nofile 100000
* hard nofile 100000
.......
</pre>

