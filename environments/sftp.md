# 搭建sftp服务器

* 创建权限组: 
> `sudo groupadd sftp`

* 创建无法登陆用户并指定权限组|设置密码: 
> `sudo useradd -g sftp -M -s /sbin/nologin sftp`    
> `sudo passwd sftp`

* 将分组统一保存目录:
> `sudo mkdir --parents /data/sftp`    
> `sudo chown -R root:sftp /data`
> `sudo chmod -R 775 /data`        
> `sudo usermod -d /data/sftp sftp`

* 停用自带sftp服务
> 编辑文件: `sudo vim /etc/ssh/sshd_config`    
> 注释该行:
>> `#Subsystem      sftp    /usr/libexec/openssh/sftp-server`   


* 启用以下配置:
<pre>
# 使用自定义sftp服务
Subsystem   sftp    internal-sftp

# 匹配用户组
Match Group sftp

# 锁定能够访问的目录: %u代表用户名,让指定用户只能访问自己目录,这里打算只需要一个用户做共享所以不需要像下面这样:
#ChrootDirectory /data/sftp/%u
ChrootDirectory /data/sftp

# 指定sftp命令
ForceCommand    internal-sftp  

# 禁用转发
AllowTcpForwarding no
X11Forwarding no
</pre>

* 重启服务
> `sudo systemctl restart sshd`


