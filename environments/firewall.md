# 防火墙加入规则

> 开放80端口 : `sudo firewall-cmd --zone=public --add-port=80/tcp --permanent` 
>> 执行完成重新加载: `sudo firewall-cmd --reload`

____

> 查看所有开放端口 : `sudo firewall-cmd --zone=public --list-ports`

____

> 删除开放规则: `sudo firewall-cmd --permanent --zone=public --remove-port=80/tcp`
/


