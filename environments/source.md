# 配置开放和更新环境

> 修改文件 : `sudo vim /etc/yum.repos.d/CentOS-Base.repo`

<pre>
[BaseOS]
name=CentOS-$releasever - Base
baseurl=https://mirrors.aliyun.com/centos/$releasever/BaseOS/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

</pre>

____

> 修改文件: `sudo vim /etc/yum.repos.d/CentOS-AppStream.repo`

<pre>
## CentOS-AppStream.repo
[AppStream]
name=CentOS-$releasever - AppStream
baseurl=https://mirrors.aliyun.com/centos/$releasever/AppStream/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

</pre>

____

> 安装RedHat标准库 : `sudo dnf install epel-release`









