安装完之后首先需要进行安全操作,这里关系到你的服务器安全最好能够优先操作来此处理.

本系统配置基于`CentOS8`系统来搭建.

> 除非十分有经验, 否则所有的服务器创建之后都必须设置安全配置

____

# 配置Selinux [可选]

`Selinux`是`Redhat`系统带的比较复杂的防护系统, 相对来说除非基本上只有专业运维才需要开启配置,不过哪怕是专业运维也很少配置该项,一般来说如果是家用个人服务器的端口转发,只需要关闭就可以.

> 一般来说`firewall-cmd`/`ufw`之类防火墙系统只开启对应某个端口即可,实际上并不需要使用到`selinux`

修改文件: `vi /etc/selinux/config`
```
#SELINUX=enforcing
SELINUX=disabled
```

> 注意,这里重启就会永久生效,但是先不要重启,将大量前置配置设置和系统更新再重启处理.

____

# 更新系统 [必须,可在配置ssh之后复制配置]

老旧的软件带来十分多的漏洞,保证系统软件最新有利于避免系统被入侵的风险.

```
$> dnf update -y
```

如果更新速度太慢,可以尝试改为阿里云的源服务:

修改`Base`源文件:

> `vi /etc/yum.repos.d/CentOS-Base.repo`

```
[BaseOS]
name=CentOS-$releasever - Base
baseurl=https://mirrors.aliyun.com/centos/$releasever/BaseOS/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
```

修改`AppStream`源文件:

> `vi /etc/yum.repos.d/CentOS-AppStream.repo`

```
[AppStream]
name=CentOS-$releasever - AppStream
baseurl=https://mirrors.aliyun.com/centos/$releasever/AppStream/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
```

配置完成后再更新即可.

安装RedHat标准库(很多软件都需要他):

> `dnf install epel-release`

____

# 更改端口|禁用Root登陆 [必须]

避免用最高的管理员登陆,`CentOS`安装系统会配置第二个账号,但是不禁用`root`账号的`SSH`登陆,需要手动禁用.

修改文件:

> `vi /etc/ssh/sshd_config`

```
# Port 22
# 修改端口为10022
Port 10022

# PermitRootLogin yes
# 禁用Root登陆
PermitRootLogin no
```

____

# 开放指定端口 [必须]

如果此时重启之后也是无法连接到`SSH`之中,需要配置防火墙协议来开放该端口.

```
# 添加修改的端口
$> firewall-cmd --zone=public --add-port=10022/tcp --permanent

# 删除原来的ssh端口
$> firewall-cmd --remove-service=ssh

# 查看开放的端口服务
$> firewall-cmd --list-all
```

____

# 设置sysctl内核配置 [可选,可在配置ssh之后复制配置]

对`Linux`内核进行微调从而让其最大效率利用系统资源.

> 在配置的时候必须了解你正在配置的操作!

修改文件: 

> `vi /etc/sysctl.conf`

```
# 注意.CentOS8默认自带了Google BBR来尽可能加速网络
net.ipv4.tcp_congestion_control=bbr
net.core.default_qdisc=fq

# 系统最大打开的文件句柄数量
fs.file-max = 65535

# 内核/网络队列占用的内存
net.ipv4.tcp_mem  = 379008 505344 758016
net.ipv4.tcp_wmem = 4096 16384 4194304
net.ipv4.tcp_rmem = 4096 87380 4194304
net.core.wmem_default = 8388608
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216

# 剩下的网络安全防溢出和转发数据配置
net.unix.max_dgram_qlen = 100
net.ipv4.tcp_syncookies=1
net.ipv4.tcp_max_syn_backlog=81920
net.ipv4.tcp_synack_retries=3
net.ipv4.tcp_syn_retries=3
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 300
net.ipv4.tcp_sack = 1
net.ipv4.tcp_fack = 1
net.ipv4.tcp_window_scaling = 1
net.ipv4.tcp_fastopen = 3
net.ipv4.ip_local_port_range = 20000 65000
net.ipv4.tcp_max_tw_buckets = 200000
net.ipv4.route.max_size = 5242880

# 内核队列和内存随机偏移来保证安全
kernel.msgmnb = 4203520
kernel.msgmni = 64
kernel.msgmax = 8192
kernel.randomize_va_space = 2
```
____

# 进程占用句柄数量 [可选,可在配置ssh之后复制配置]

修改文件: 
> `vi /etc/security/limits.conf`

```
# 数量和file-max一致
* soft nofile 65535
* hard nofile 65535
```

____


至此基本上就完成初步的配置,后续就是`ssh`的密钥登陆等问题,至此先重启来生效配置,在`ssh`登陆测试.