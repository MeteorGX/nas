#!/usr/bin/bash

if [ `whoami` != "root" ];then
    echo "Error! Permission denied"
    exit 1;
fi



sys_ctl="/usr/sbin/sysctl"
restart_ctr="/usr/sbin/reboot"


$sys_ctl -a | grep bbr
if [ $? != 0 ];then
    echo "Not Found BBR"
    echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
    echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
    
    $sys_ctl -p 
    echo "Restart?[Y]"
    read cmd
    if [ $cmd = "Y" -o $cmd = "y" ];then
        $restart_ctr
    fi
fi

