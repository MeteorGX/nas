`MongoDB`的文档型内存数据库,其中使用类似于`Json`的`Bson`格式结构; 相比较`Redis`能够进行更加灵活的表格式,但是当数据量上去之后占用也是十分庞大.

# 安装Mongo(基于CentOS8)

目前CentOS8并没有带有包,所以需要自己配置的MongoDB源,这个是官方源但是速度不理想:

> 编辑源: `vi /etc/yum.repos.d/mongodb.repo`

```
[mongodb-org]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/development/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
```

`dnf`安装`Mongod`:

```
$> dnf install mongodb-org
```

设置启动并且开机重启

```
$> systemctl start mongod.service
$> systemctl enable mongod.service
```

____

# 配置Mongod

> 修改文件: `/etc/mongod.conf`

```
net:
  port: 27017
  #bindIp: 127.0.0.1
  #不推荐使用0.0.0.0,Mongo的对外公网安全过滤很薄弱,最好是在内网做处理,不要暴露公网
  bindIp: 192.168.0.x

#这里暂时不要启动,需要后续的创建管理员账户再打开
security:
  authorization: "enabled"
```

配置完之后就需要创建一个用户来进行管理

```
#进入管理界面
$> mongo

#创建高级管理用户
use admin;
db.createUser({
    user: "meteorcat",
    pwd: "qwe123456",
    roles: [ { role: "root", db: "admin" } ]
});

#低级管理账户
use admin;
db.createUser({
    user: "meteorcat",
    pwd: "qwe123456",
    roles: [ { role: "readWrite", db: "数据库名称" } ]
});

#首先创建一个数据库准备后续的备份数据
#use 数据库名,如果没有数据库会直接创建,同时没有数据也是无法通过show dbs查看
use dev;
db.dev.insert({"name":"meteorcat","content":"hello.world"});
```

开启安全设置重启`Mongod`就只允许登陆用户登陆才能操作了.

____

# 备份/还原数据

`Mongod`备份数据有自带的命令可以处理:

```
#备份处理
#-h表示地址 -d表示数据库名称 -o 表示出书的目录
#-u表示账号名 -p表示密码  --gzip 表示输出成压缩格式
#--authenticationDatabase "admin" 这个是指定权限验证数据,我们创建账号都是基于admin数据库,对应你账号的所属库
$> /usr/bin/mongodump -h 127.0.0.1:27017 -d dev --authenticationDatabase "admin" -u meteorcat -p qwe123456 --gzip -o /tmp/mongo


#还原处理
#--drop表示会先删除数据再导入 
$> /usr/bin/mongorestore --drop -h 127.0.0.1:27017 --authenticationDatabase "admin" -u meteorcat -p qwe123456 --gzip /tmp/mongo
```

____

至此`Mongod`已经架设部署完成.



