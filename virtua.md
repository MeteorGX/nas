市面上目前的服务器虚拟化方案:

* [`VMware ESXI`](https://my.vmware.com/cn/web/vmware/evalcenter?p=free-esxi6)

* [`Proxmox`](https://www.proxmox.com)

* [``CentOS8自带的`cockpit/cockpit-machine` ``](https://www.linuxtechi.com/install-use-cockpit-tool-centos8-rhel8/)

* [`Window方案hyper-v`](https://docs.microsoft.com/zh-cn/virtualization/hyper-v-on-windows/about/)

以下是选择虚拟化方案注意点:

* `ESXI`并不是免费项目,个人使用在一般配置上是免费的,到达一定配置或者使用量会进行收费.

* `Proxmox`是开源免费项目,适合不打算提供维护虚拟化费用支出使用者.

* `cockpit-machine`实际上的简化版虚拟机(libvirt)构建, 一般可以用于单台少量简单管理的机器. `CentOS8`带了该运行包.

* `hyper-v`虚拟化一般要求window服务器环境,也就是宿主机必须`Window`系统.

按照自己需求选择虚拟化方案之后开始部署环境,以下提供虚拟化搭建工具/流程:

* 镜像写入工具: [`Rufus`](https://rufus.ie/)

* 搭建`Proxmox`: [`参考`](https://post.smzdm.com/p/768830/)

* 搭建`cockpit-machine-1`: [`参考`](https://linux.cn/article-11716-1.html)

* 搭建`cockpit-machine-2`: [`参考`](https://www.kclouder.cn/howtocockpit/)

____
